# Activity
# Create a Class called Camper and give it the attributes name, batch, course_type
# Create a method called career_track which will print out the string Currently enrolled in the <value of course_type> program.
# Create a method called info which will print out the string My name is <value of name> of batch <value of batch>.
# Create an object from class Camper called zuitt_camper and pass in arguments for name, batch, and course_type.
# Print the value of the object's name, batch, course type.
# Execute the info method and career_track of the object.


class Camper():
	def __init__(self, name, batch, course_type):
		self.name = name
		self.batch = batch
		self.course_type = course_type

	# method
	def career_track(self):
		print(f"Currently enrolled in the {self.course_type} program.")

	def info(self):
		print(f"My name is {self.name} of batch {self.batch}.")

zuitt_camper = Camper("Leizl", "276", "Intro to Python")

print(f"Camper Name: {zuitt_camper.name}")
print(f"Camper Batch: {zuitt_camper.batch}")
print(f"Camper Course: {zuitt_camper.course_type}")

zuitt_camper.info()
zuitt_camper.career_track()
